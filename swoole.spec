%global ini_name 40-swoole.ini

Name:		swoole
Version:	5.1.2
Release:	1
Summary:        Event-driven asynchronous and concurrent networking engine with high performance for PHP
License:        Apache 2.0
URL:            https://www.swoole.com/
Source0:        https://github.com/swoole/swoole-src/archive/refs/tags/v%{version}.tar.gz
BuildRequires:	php(language) >= 8.0
BuildRequires:	php-cli >= 8.0
BuildRequires:  php-devel
BuildRequires:	openssl-devel >= 1.0.2
BuildRequires:	curl-devel
BuildRequires:	c-ares-devel
BuildRequires:	libnghttp2-devel
BuildRequires:	brotli-devel
BuildRequires:	libpq-devel
Requires:	php(api) = %{php_core_api}
Requires:	php-curl

%description
Event-driven asynchronous and concurrent networking engine with high performance for PHP.
	- event-driven
	- coroutine
	- asynchronous non-blocking
	- multi-thread reactor
	- multi-process worker
	- multi-protocol
	- millisecond timer
	- built-in tcp/http/websocket/http2 server
	- coroutine tcp/http/websocket client
	- coroutine mysql client
	- coroutine redis client
	- coroutine read/write file system
	- coroutine dns lookup
	- support IPv4/IPv6/UnixSocket/TCP/UDP
	- support SSL/TLS encrypted transmission

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}
Requires: php-devel >= 8.0

%description devel
Development files for %{name}.

%prep
%autosetup -p1 -n swoole-src-%{version}

# Drop in the bit of configuration
cat << 'EOF' | tee %{ini_name}
; Enable swoole extension module
extension = swoole.so
EOF

%build
%{_bindir}/phpize
%configure \
	--enable-openssl \
	--with-openssl-dir=%{_prefix} \
	--with-brotli-dir=%{_prefix} \
	--enable-cares \
	--enable-swoole-curl \
	--with-nghttp2-dir=%{_prefix} \
	--enable-swoole-pgsql
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

install -D -m 644 %{ini_name} %{buildroot}%{php_inidir}/%{ini_name}

%files
%license LICENSE
%{php_inidir}/%{ini_name}
%{_libdir}/php/modules/swoole.so

%files devel
%{_includedir}/php/ext/swoole

%changelog
* Sun Apr 21 2024 Funda Wang <fundawang@yeah.net> - 5.1.2-1
- 5.1.2

* Mon Aug 21 2023 Funda Wang <fundawang@yeah.net> - 5.0.3-1
- Import package
